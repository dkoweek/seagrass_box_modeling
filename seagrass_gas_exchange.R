gas_exchange <- function(u, h, T, S, pCO2) {
  #Input arguments:
  #u: water velocity (m/d)
  #h: water depth (m)
  #T: water temperature (deg C)
  #S: salinity
  #pCO2: aqueous pCO2 (uatm)
  
  #Equations for gas transfer velocity
  u <- abs(u) * 100 / 86400 #Take absolute value of velocity (i.e. gas transfer velocity dependence on flow does not depend on direction of flow)
                            #convert from m/d to cm/s (most gas transfer velocity parameterizations accept u in units of cm/s)
  
  
  #Borges et al. (2004) "Variability of the Gas Transfer Velocity of CO2 in a Macrotidal Estuary (the Scheldt)" Estuaries 27(4) 593-603
  #Eq. 12 in Borges et al.
  k600 <- 1.719 * (u ^ 0.5) * (h ^ -0.5) #cm/hr
  k600 <- k600 / 100 #m/hr
  
  #Ho et al. (2014) "Influence of current velocity and wind speed on air-water gas exchange in a mangrove estuary" Geophysical Research Letters 43 3813-3821
  #Eq. 7 in Ho et al. (modified to remove wind component)
  #   k600<-0.77*(u^0.5)*h^(-0.5) #cm/hr
  #   k600<-k600/100 #m/hr
  
  #Need Schmidt number for CO2 to convert from k600 to kCO2
  Sc_coefs <- c(2073.1, 125.62, 3.6276, 0.043219)
  Sc_CO2 <-
    Sc_coefs[1] - (Sc_coefs[2] * T) + (Sc_coefs[3] * (T ^ 2)) - (Sc_coefs[4] *
                                                                   (T ^ 3))
  
  #Convert k600 to kCO2
  kCO2 <- k600 * ((600 / Sc_CO2) ^ 0.5) #m/hr
  
  #Now calculate the solubility of CO2
  T_K <- T + 273.15 #Convert from deg C to K
  A_CO2 <- c(-160.7333, 215.4152, 89.8920, -1.47759)
  B_CO2 <- c(0.029941, -0.027455, 0.0053407)
  F_CO2 <-
    exp((A_CO2[1] + (A_CO2[2] * (100 / T_K)) + (A_CO2[3] * (log(
      T_K / 100
    ))) + (A_CO2[4] * ((
      T_K / 100
    ) ^ 2))) +
      (S * (B_CO2[1] + (B_CO2[2] * (
        T_K / 100
      )) + (B_CO2[3] * ((T_K / 100) ^ 2
      )))))
  pH2O_to_P = exp(24.4543 - 67.4509 * (100 / T_K) - 4.8489 * log(T_K / 100) -
                    0.000544 * S)
  P_surface <- 1 #atm
  S_CO2 = (F_CO2 / (P_surface - (pH2O_to_P * P_surface))) * 1e6 #mmol/m^3/atm
  
  #Calculate air/sea gas flux
  pCO2_atm <- 400 / 1e6 #atm
  J <- kCO2 * S_CO2 * (pCO2 / 1e6 - pCO2_atm) #mmol/m^2/hr
  J <- J * 24 #mmol/m^2/d
  
  return(J)
}