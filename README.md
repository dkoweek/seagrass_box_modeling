# This repository contains the source code needed to run the seagrass model. It contains a series of scripts and functions explained below.
# README written by David Koweek 12 July 2017

seagrass_box_model.R - script for executing the seagrass numerical model. User edits the script as indicated in the comment notes to set model parameters where necessary.

seagrass_box_model_plots.R - plotting script for generating multi-panel plot of model results. Useful as a quick diagnostic of model results. Can automatically be sourced when sourcing ‘seagrass_box_model.R’ by setting ‘plot_results’ to TRUE.

seagrass_gas_exchange.R - function for calculating air/sea gas exchange using gas transfer velocity parameterizations for the coastal zone. If this script is sourced, gas transfer fluxes will be considered in the seagrass meadow DIC mass balance.

seagrass_metabolic_rates.R - custom function for calculating gross production and respiration from carbonate chemistry, light, and leaf area index (LAI). Switches between summer and winter metabolic rates based on temperature input.

seagrass_ODEs.R - custom function for solving ODEs (DIC and TA mass balances) when assuming fixed boundaries. Called automatically by ODE solver when ‘sloshing’ is set to FALSE in the model.

seagreass_ODEs_sloshing.R - custom function for solving ODEs (DIC and TA mass balances) when assuming side boxes. Called automatically by ODE solver when ‘sloshing’ is set to TRUE in the model.

seagrass_steady_state_analysis.R - script for producing steady-state solution figure to seagrass meadow DIC smass balance equation. Figure aesthetics may not be identical to figure presented in the manuscript.

