#Written by David Koweek to visualize model output of seagrass box model

#Source necessary packages----
library(ggplot2)
library(cowplot)

#Create data frame of all variables for plotting----
df <-
  data.frame(
    Time = t,
    Depth = h,
    Ed = Ed,
    u = u_in,
    GPP = Pg,
    R = R,
    NCP = Pg - R,
    DIC = DIC_out,
    TA = TA_out,
    pH = pH_out,
    pCO2 = pCO2_out,
    Omega_Ar = Omega_Ar_out
  ) 

#Create time series plots----

#Plot settings
axis <- ggplot(data = df, aes(x = Time))
details <-
  theme(
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.background = element_blank(),
    axis.line.x = element_line(colour = "black"),
    axis.line.y = element_line(colour = "black")
  )
plot <- axis + details

#Generate time series plots
h_plot <- plot + geom_line(aes(y = Depth)) + ylab(expression(Depth(m)))

E_plot <- plot +
          geom_line(aes(y = Ed)) +
          ylab(expression(E ~ (mu ~ mol ~ m ^ {
            -2
          } ~ s ^ {
            -1
          })))

u_plot <- plot +
          geom_line(aes(y=u/86400)) + #Convert from m/d to m/s
          geom_hline(yintercept = u_bar, colour = "green") +
          ylab(expression(Water~Velocity~(m~s^{-1})))

metab_plot <- plot +
            geom_line(aes(y = GPP, 
                          colour = "GPP")) + 
            geom_line(aes(y = R, 
                          colour = "R")) + 
            geom_line(aes(y = NCP, 
                          colour = "NCP")) + 
            scale_colour_manual(name = element_blank(),
                                values = c(GPP = "red", 
                                           R = "blue", 
                                           NCP = "black")) + 
            labs(y = expression(paste(mmol ~ C ~ m ^ {-2} ~ hr ^ {-1}))) + 
            ylim(c(-5, 30)) + 
            theme(legend.direction = "horizontal", 
                  legend.position = c(0.5, 0.85))

DIC_plot <- plot + 
          geom_line(aes(y = DIC_out)) + 
          geom_hline(yintercept = DIC_ocean*1000, colour="blue") +
          geom_hline(yintercept = DIC_estuary*1000, colour="red") +
          labs(y = expression(paste(DIC ~ ( ~ mu ~ mol ~ kg ^ {-1}))))

TA_plot <- plot +
            geom_line(aes(y = TA_out)) + 
            geom_hline(yintercept = TA_ocean*1000, colour="blue") +
            geom_hline(yintercept = TA_estuary*1000, colour="red")+
            labs(y = expression(paste(TA ~ ( ~ mu ~ mol ~ kg ^ {-1}))))

pH_plot <- plot +
            geom_line(aes(y = pH_out)) +
            geom_hline(yintercept = pH_ocean, colour = "blue") +
            geom_hline(yintercept = pH_estuary, colour = "red") +
            labs(y = expression(pH ~ (total ~ scale))) 

pCO2_plot <- plot + 
              geom_line(aes(y = pCO2_out)) +
              geom_hline(yintercept = carb(flag = 8,
                                            var1 = pH_ocean,
                                            var2 = TA_ocean / 1e3,
                                            T = T,
                                            S = S)$pCO2,
                         colour = "blue") +
              geom_hline(yintercept = carb(flag = 8,
                                          var1 = pH_estuary,
                                          var2 = TA_estuary / 1e3,
                                          T = T,
                                          S = S)$pCO2,
                         colour = "red") +
              labs(y = expression(pCO[2] ~ (mu ~ atm)))
              
Omega_Ar_plot <- plot + 
                 geom_line(aes(y = Omega_Ar_out)) +
                 geom_hline(yintercept = carb(flag = 8,
                                               var1 = pH_ocean,
                                               var2 = TA_ocean / 1e3,
                                               T = T,
                                               S = S)$OmegaAragonite,
                             colour = "blue") +
                  geom_hline(yintercept = carb(flag = 8,
                                               var1 = pH_estuary,
                                               var2 = TA_estuary / 1e3,
                                               T = T,
                                               S = S)$OmegaAragonite,
                             colour = "red") +    
                  labs(y = expression(Omega[Ar])) 

#Save multipanel plots----

#Point to subdirectory with model plots
setwd("~/Documents/General C cycling/BML_seagrass_workshop/Seagrass_modeling/Box_Model/Model_output/Plots/")

#Assemble multi-panel plot
box_model_plot <- plot_grid(h_plot,
                            E_plot,
                            u_plot,
                            metab_plot,
                            DIC_plot,
                            TA_plot,
                            pH_plot,
                            pCO2_plot,
                            Omega_Ar_plot, 
                            ncol = 3)

#Add model details to bottom of plot
box_model_plot <- box_model_plot %>%
                    add_sub(str_c(
                              str_c("Start=Day", CD_0, sep=" "),
                              str_c("T=",T,"C", sep=""),
                              str_c("S=",S,sep=""),
                              str_c("LAI=", LAI, sep=""),
                              str_c("phi=", phase_shift*24," hours", sep=""),
                              "ocean=blue, estuary=red",
                              str_c("sloshing=",if(sloshing==TRUE){"ON"} else {"OFF"},sep=""),
                              sep=", ")) %>%
                    ggdraw
         
cowplot::ggsave(filename = str_c(filename,
                                 ".eps"),
                plot = box_model_plot,
                width = 11,
                height = 8,
                units = "in")
